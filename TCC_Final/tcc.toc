\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{21}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{23}{section.1.1}
\contentsline {section}{\numberline {1.2}Organiza\IeC {\c c}\IeC {\~a}o do trabalho}{23}{section.1.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Processos}}{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Defini\IeC {\c c}\IeC {\~a}o e gerenciamento de processo}{25}{section.2.1}
\contentsline {section}{\numberline {2.2}Automatiza\IeC {\c c}\IeC {\~a}o dos processos por meio da Tecnologia da Informa\IeC {\c c}\IeC {\~a}o}{26}{section.2.2}
\contentsline {section}{\numberline {2.3}Processos na ag\IeC {\^e}ncia de publicidade digital}{26}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Proposta de sistematiza\IeC {\c c}\IeC {\~a}o dos processos}}{37}{chapter.3}
\contentsline {section}{\numberline {3.1}Defini\IeC {\c c}\IeC {\~a}o da plataforma}{37}{section.3.1}
\contentsline {section}{\numberline {3.2}Funcionalidades da plataforma}{37}{section.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Considera\IeC {\c c}\IeC {\~o}es parciais}}{41}{chapter.4}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{43}{section*.11}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{45}{section*.12}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {\IeC {\'A}reas de uma ag\IeC {\^e}ncia digital}}{47}{appendix.A}
\contentsline {section}{\numberline {A.1}P\IeC {\'a}peis e fun\IeC {\c c}\IeC {\~o}es em uma ag\IeC {\^e}ncia digital}{50}{section.A.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Documenta\IeC {\c c}\IeC {\~a}o da Plataforma}}{53}{appendix.B}
\contentsline {section}{\numberline {B.1}\textit {Users Stories}}{53}{section.B.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}\MakeTextUppercase {Telas da plataforma}}{57}{appendix.C}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {D}\MakeTextUppercase {Cronograma detalhado}}{61}{appendix.D}
